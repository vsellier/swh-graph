/// Structures to load file created with legacy swh-graph implementation
/// written in Java.
///
/// `mph`, and `sf` are imported [from sux-rs](https://archive.softwareheritage.org/swh:1:dir:5aef0244039204e3cbe1424f645c9eadcc80956f;origin=https://github.com/vigna/sux-rs;visit=swh:1:snp:855180f9102fd3d7451e98f293cdd90cff7f17d9;anchor=swh:1:rev:9cafac06c95c2d916b76dc374a6f9d976bf65456)
pub mod bit_vector;
pub mod fcl;
pub mod mph;
pub mod sf;
